﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DataTableCrud.Models
{
    [MetadataType(typeof(EmployeeMetadata))]
    public partial class Employees
    {

    }
    public class EmployeeMetadata
    {
        [Required (AllowEmptyStrings = false,ErrorMessage ="Por favor introduce el nombre")]
        public string FirstName { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Por favor introduce el apellido")]
        public string LastName { get; set; }
    }
}