USE appdb
CREATE TABLE [dbo].[Employees]
(
	[EmployeeID] INT NOT NULL PRIMARY KEY Identity,
	[FirstName] nvarchar(50) not null,
	[LastName] nvarchar(50) not null,
	[EmailID] nvarchar(200),
	[City] nvarchar(50),
	[Country] nvarchar(50)
)